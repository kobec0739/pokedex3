//
//  Constants.swift
//  pokedex3
//
//  Created by Tim Chen on 3/18/17.
//  Copyright © 2017 Tim Chen. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co/"
let URL_POKEMON = "/api/v1/pokemon/"

typealias DownloadComplete = () -> ()
