//
//  Pokemon.swift
//  pokedex3
//
//  Created by Tim Chen on 3/14/17.
//  Copyright © 2017 Tim Chen. All rights reserved.
//

import Foundation
import Alamofire

class Pokemon {
    fileprivate var _name: String!
    fileprivate var _pokedexId: Int!
    private var _description: String!
    private var _type: String!
    private var _defense: String!
    private var _height: String!
    private var _weight: String!
    private var _attack: String!
    private var _nextEvolutionTxt: String!
    private var _nextEvolutionName: String!
    private var _nextEvolutionId: String!
    private var _nextEvolutionLevel: String!
    
    private var _pokemonURL: String!
    
    var nextEvolutionLevel: String {
        if _nextEvolutionLevel == nil {
            _nextEvolutionLevel = ""
        }
        return _nextEvolutionLevel
    }
    
    var nextEvolutionId: String {
        if _nextEvolutionId == nil {
            _nextEvolutionId = ""
        }
        return _nextEvolutionId
    }
    
    var nextEvolutionName: String {
        if _nextEvolutionName == nil {
            _nextEvolutionName = ""
        }
        return _nextEvolutionName
    }
    
    var nextEvolutionTxt: String {
        if _nextEvolutionTxt == nil {
            _nextEvolutionTxt = ""
        }
        return _nextEvolutionTxt
    }
    
    var attack: String {
        if _attack == nil {
            _attack = ""
        }
        return _attack
    }
    
    var weight: String {
        if _weight == nil {
            _weight = ""
        }
        
        return _weight
    }
    
    var height: String {
        if _height == nil {
            _height = ""
        }
        
        return _height
    }
    
    var defense: String {
        if _defense == nil {
            _defense = ""
        }
        
        return _defense
    }
    
    var type: String {
        if _type == nil {
            _type = ""
        }
        
        return _type
    }

    var description: String {
        if _description == nil {
            _description = ""
        }
        
        return _description
    }

    
    var name: String {
        return self._name
    }
    
    var pokedexId: Int {
        return self._pokedexId
    }
    
    init (name: String, pokedexId: Int) {
        self._name = name
        self._pokedexId = pokedexId
        
        self._pokemonURL = "\(URL_BASE)\(URL_POKEMON)\(self.pokedexId)/"
    }
    
    // only when we click the pokemon and go to detail page we call api
    func downloadPokemonDetails(completed: @escaping DownloadComplete) {
        Alamofire.request(self._pokemonURL).responseJSON { (response) in
            
            if let dic = response.result.value as? Dictionary<String, AnyObject> {
                if let weight = dic["weight"] as? String {
                    self._weight = weight
                }
                
                if let height = dic["height"] as? String {
                    self._height = height
                }
                
                if let attack = dic["attack"] as? Int {
                    self._attack = "\(attack)"
                }
                
                if let defense = dic["defense"] as? Int {
                    self._defense = "\(defense)"
                }
                
                if let types = dic["types"] as? [Dictionary<String, String>] , types.count > 0 {
                    
                    if let name = types[0]["name"] {
                        self._type = name.capitalized
                    }
                    
                    if types.count > 1 {
                        for i in 1 ..< types.count {
                            if let name = types[i]["name"] {
//                                self._type += "/\(name.capitalized)"
                                self._type.append("/\(name.capitalized)")
                            }
                        }
                    }
                    
                    
                } else {
                    self._type = ""
                }
                
                if let desArr = dic["descriptions"] as? [Dictionary<String, String>] , desArr.count > 0 {
                    if let url = desArr[0]["resource_uri"] {
                        let descURL = "\(URL_BASE)\(url)"
                        Alamofire.request(descURL).responseJSON(completionHandler: {(response) in
                            if let descDic = response.result.value as? Dictionary<String, AnyObject> {
                                if let desc = descDic["description"] as? String {
                                    self._description = desc
                                    
                                    print(desc)
                                }
                            }
                            
                            completed()
                        })
                    }
                } else {
                    self._description = ""
                }
                
                if let evolutions = dic["evolutions"] as? [Dictionary<String, AnyObject>] , evolutions.count > 0 {
                    if let nextEvo = evolutions[0]["to"] as? String {
                        if nextEvo.range(of: "mega") == nil {
                            self._nextEvolutionName = nextEvo
                            
                            // acquring evolution id from uri
                            if let uri = evolutions[0]["resource_uri"] as? String {
                                let newStr = uri.replacingOccurrences(of: "/api/v1/pokemon/", with: "")
                                let nextEvoId = newStr.replacingOccurrences(of: "/", with: "")
                                self._nextEvolutionId = nextEvoId
                            }
                            
                            if let lvlExist = evolutions[0]["level"] {
                                if let lvl = lvlExist as? Int {
                                    self._nextEvolutionLevel = "\(lvl)"
                                }
                            } else {
                                self._nextEvolutionLevel = ""
                            }
                        }
                    }
                    
                    
                }
            }
            
            completed()
            
        }
            
        
    }
    
}
