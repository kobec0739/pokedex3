//
//  PokemonDetailVC.swift
//  pokedex3
//
//  Created by Tim Chen on 3/16/17.
//  Copyright © 2017 Tim Chen. All rights reserved.
//

import UIKit

class PokemonDetailVC: UIViewController {
    
    var poke: Pokemon!

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var defenceLbl: UILabel!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var pokedexLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var attackLbl: UILabel!
    @IBOutlet weak var currentEvoImg: UIImageView!
    @IBOutlet weak var nextEvoImg: UIImageView!
    @IBOutlet weak var evoLbl: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLbl.text = poke.name.capitalized
        
        let img = UIImage(named: "\(poke.pokedexId)")
        
        mainImg.image = img
        currentEvoImg.image = img

       poke.downloadPokemonDetails {
        
            print("Got here")
            // what ever we write will only be called after the network call is complete!
            self.updateUI()
        }
    }

    @IBAction func backBtnPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func updateUI() {
        attackLbl.text = poke.attack
        defenceLbl.text = poke.defense
        heightLbl.text = poke.height
        weightLbl.text = poke.weight
        typeLbl.text = poke.type
        descriptionLbl.text = poke.description
        
        if poke.nextEvolutionId == ""
        {
            evoLbl.text = "No Evolutions"
            nextEvoImg.isHidden = true
        } else {
            nextEvoImg.isHidden = false
            nextEvoImg.image = UIImage(named: poke.nextEvolutionId)
            
            let str = "Next Evolution: \(poke.nextEvolutionName) - LVL \(poke.nextEvolutionLevel)"
            evoLbl.text = str
        }
    }

}
